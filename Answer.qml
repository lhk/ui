import QtQuick 2.1
import QtQuick.Controls 1.0


Rectangle{
    id: answer
    property color editColor: "grey"
    property color rightColor: "green"
    property color wrongColor: "red"
    /*property int pos_x:100
    property int pos_y: 200
    x:pos_x
    y:pos_y*/
    color:editColor
    width:200
    height:50
    radius:10
    TextField{
        text:"answer"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.fill: parent
        anchors.margins: 10
    }
    /*
    MouseArea{
        anchors.fill:parent
        onClicked: {
            console.log("clicked");
            wrongAnswer();
        }
    }*/


    signal wrongAnswer()
    onWrongAnswer: {
        wronganswer.start();
    }

    signal rightAnswer()
    onRightAnswer: {
        rightanswer.start();
    }

    PropertyAnimation{
        id:rightanswer
        target: answer
        properties:"color"
        to:answer.rightColor
        duration:200
        onRunningChanged: {
            if(!running)backtonormal.start();
        }
    }

    PropertyAnimation{
        id:wronganswer
        target: answer
        properties: "color"
        to:answer.wrongColor
        duration:200
        onRunningChanged: {
            if(!running)backtonormal.start();
        }
    }

    PropertyAnimation{
        id: backtonormal
        target: answer
        properties: "color"
        to: answer.editColor
        duration: 200
        onRunningChanged: {
            console.log("finished animating")
        }
    }
}

import QtQuick 2.1
import QtQuick.Controls 1.0

Rectangle{
    id: question
    property int pos_x: 200
    property int movement_x: 200
    property int pos_y: 100
    property string text: "Question"
    width: 200
    height: 20
    color:"lightgrey"
    x: pos_x
    y: pos_y
    radius:50

    signal newQuestion()

    onNewQuestion: {
        flyout.start();
    }

    Label{
        id:label
        opacity:parent.opacity
        anchors.fill: parent
        text: "set new text by changing text property and calling newQuestion"
        MouseArea{
            onClicked: {
                flyout.start();
            }
            anchors.fill: parent
        }
    }

    ParallelAnimation{
        id: flyout
        NumberAnimation {
            target:question
            properties: "x";
            to: question.pos_x+question.movement_x
            duration: 500
        }
        NumberAnimation{
            target: question
            properties: "opacity"
            to:0
            duration: 400
        }
        onRunningChanged: {
            if(!running){
                question.x=question.pos_x-question.movement_x;
                label.text=question.text;
                flyin.start();
            }
        }
    }
    ParallelAnimation{
        id: flyin
        NumberAnimation {
            target:question
            properties: "x";
            to: question.pos_x
            duration: 500
        }
        NumberAnimation{
            target: question
            properties: "opacity"
            to:1
            duration: 400
        }
    }

}

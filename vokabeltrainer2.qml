import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Window 2.0

ApplicationWindow {
    title: qsTr("Hello World")
    width: 500
    height: 150

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    Question{
        id:question
        anchors.bottom:answer.top
        pos_x:(parent.width-width)/2
        width:parent.width-100
    }

    Answer{
        id:answer
        width:500
        anchors.bottom: checkbutton.top
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Button{
        id:checkbutton
        text:"check"
        anchors.bottom:parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width:answer.width
        onClicked: {
            answer.wrongAnswer();
            question.newQuestion();
            question.text="new text";
        }
    }

}
